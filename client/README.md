# Orbit Component :: ZeroDocs

#### This component provides in-line documentation based on remote Git repositories (currently GitLab is supported). Content from the repository (Python and Markdown files) will be retreieved, reformatted and displayed on-the-fly based on a chosen branch.

