import DBShell from '@/../node_modules/orbit-component-dbshell';
import DataArrayOutlined from '@vicons/material/DataArrayOutlined.js'
import AlternateEmailOutlined from '@vicons/material/AlternateEmailOutlined.js'
import { shallowRef } from 'vue'

export function menu (app, router, menu) {
    const IconDBShell = shallowRef(DataArrayOutlined)
    const IconShell = shallowRef(AlternateEmailOutlined)
    app.use(DBShell, {
        router: router,
        menu: menu,
        buttons: [
            {name: 'dbshell1', text: 'DB Shell'  , component: DBShell , icon: IconDBShell , pri: 4, path: '/dbshell', key: '12', meta: {
                root: 'db1',
                host: location.host,
                exec: 'orbit_database_shell',
                endp: 'on_mount'
            }},
            {name: 'dbshell2', text: 'Shell'    , component: DBShell , icon: IconShell , pri: 5, path: '/shell', key: '13', meta: {
                root: 'db2',
                host: location.host,
                exec: 'bash',
                disabled: 'hide',
                endp: 'on_mount'
            }},
        ]
    })
};
