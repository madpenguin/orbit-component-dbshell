from orbit_component_base.src.orbit_plugin import ArgsBase
from orbit_component_base.src.orbit_shells import PluginXTerm
from orbit_component_base.src.orbit_decorators import Sentry, check_permission
from loguru import logger as log

class Plugin (PluginXTerm):

    NAMESPACE = 'dbshell'
    WHITELIST = ['orbit_database_shell']

    @Sentry(check_permission, NAMESPACE, 'User is allowed to open a new terminal session')
    async def on_mount (self, sid, root, executable):
        if executable in self.WHITELIST:
            return await super().mount(sid, root, executable)
        return {'ok': False, 'error': f'{executable} not allowed to run, see WHITELIST'}


class Args (ArgsBase):

    def setup (self):
        self._parser.add_argument("--dbs-whitelist", nargs='+', type=str, metavar=('EXECUTABLE'), help='Whitelisted executables for the dbshell module')
        return self


class Tasks (ArgsBase):
    
    async def process (self):
        if self._args.dbs_whitelist:
            Plugin.WHITELIST = self._args.dbs_whitelist
